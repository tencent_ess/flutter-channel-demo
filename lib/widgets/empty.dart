import 'package:flutter/material.dart';

class Empty extends StatelessWidget {
  const Empty({super.key, this.emptyLabel});
  final String? emptyLabel;

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
      padding: const EdgeInsets.all(10),
      child: Column(
        children: [
          Image.network(
            'https://img01.yzcdn.cn/vant/empty-image-default.png',
            width: 160,
            height: 160,
          ),
          Text(
            emptyLabel ?? '暂无数据',
            style: const TextStyle(color: Color(0xff969799), fontSize: 14),
          ),
        ],
      ),
    ));
  }
}
