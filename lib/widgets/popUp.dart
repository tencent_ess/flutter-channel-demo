import 'package:flutter/material.dart';

void showAlertPopup(BuildContext context, String title, String detail,
    String buttonText) async {
  void showDemoDialog<T>(
      {required BuildContext context, required Widget child}) {
    showDialog<T>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) => child,
    );
  }

  return showDemoDialog<Null>(
      context: context,
      child: AlertDialog(
        title: Text(title),
        content: Text(detail),
        actions: [
          TextButton(
              child: Text(buttonText),
              onPressed: () {
                Navigator.pop(context);
              }),
        ],
      ));
}
