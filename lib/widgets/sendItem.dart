import 'package:flutter/material.dart';

//派遣记录item
Widget sendItem(
    {context, List? sendList, int? index, Map<String, dynamic>? item}) {
  dynamic target = item ?? sendList![index!];
  String startTime = target['startTime'].substring(0, 10);
  String endTime = target['endTime'].substring(0, 10);
  String serviceText = '服务时间：$startTime ~ $endTime';
  return Container(
    padding: const EdgeInsets.only(
      top: 12.0,
      bottom: 12.0,
    ),
    margin: const EdgeInsets.only(left: 16.0, right: 16.0),
    decoration: const BoxDecoration(
        border: Border(
      bottom: BorderSide(width: 1.0, color: Color(0xffEBEDF0)),
    )),
    child: Column(
      textDirection: TextDirection.ltr,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Row(
          children: [
            Text(target['tenantName'] ?? '',
                textAlign: TextAlign.left,
                style: const TextStyle(
                    fontSize: 14.0, fontWeight: FontWeight.bold, height: 1.5)),
            SizedBox(
              child: recordStatusBuilder(target),
            )
          ],
        ),
        Row(
          children: [
            Text(serviceText,
                textAlign: TextAlign.left,
                style: const TextStyle(fontSize: 12, height: 2.0))
          ],
        )
      ],
    ),
  );
}

Widget recordStatusBuilder(target) {
  List<Widget> children = [];
  String statusText = '服务中';
  Color statusColor = const Color(0xff07c160);

  // Map<String, dynamic> target = sendList[index];
  String startTime = target['startTime'].substring(0, 10);
  String endTime = target['endTime'].substring(0, 10);
  bool isService = false;
  dynamic remainDays = 0; ////剩余服务天数
  DateTime now = DateTime.now();
  if (DateTime.parse(startTime).isAfter(now)) {
    statusText = '未开始';
    statusColor = Colors.lightBlue;
  } else if (DateTime.parse(endTime).isBefore(now)) {
    //已结束
    statusText = '已结束';
    statusColor = const Color(0xffcccccc);
  } else {
    //服务中
    isService = true;
    remainDays = DateTime.parse(endTime).difference(now).inDays;
  }

  children.addAll([
    Container(
      padding: const EdgeInsets.fromLTRB(5.0, 2.0, 10.0, 2.0),
      margin: const EdgeInsets.only(left: 8.0, right: 5.0),
      decoration: BoxDecoration(
          color: statusColor,
          borderRadius: const BorderRadius.only(
              topRight: Radius.circular(10.0),
              bottomRight: Radius.circular(10.0))),
      child: Text(
        statusText,
        style: const TextStyle(color: Colors.white, fontSize: 10.0),
      ),
    ),
  ]);

  if (isService) {
    children.addAll([
      const Icon(
        Icons.alarm_outlined,
        color: Colors.grey,
        size: 16,
      ),
      Text(
        ' 剩余 $remainDays 天',
        style: const TextStyle(fontSize: 12.0),
      ),
    ]);
  }

  return Row(
    children: children,
  );
}
