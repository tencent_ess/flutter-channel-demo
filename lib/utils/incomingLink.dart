import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:uni_links/uni_links.dart';
import './tools.dart';

// 处理外部跳转到app内监听逻辑
void incomingLinkHandler(streamSubscription, mounted, Function setCurrentURI) {
  // 1
  if (!kIsWeb) {
    // 2
    streamSubscription = uriLinkStream.listen((Uri? uri) {
      if (!mounted) {
        return;
      }
      debugPrint('Received URI: $uri');
      setCurrentURI(uri);

      // 3
    }, onError: (Object err) {
      if (!mounted) {
        return;
      }
      debugPrint('Error occurred: $err');
      setCurrentURI(null);
    });
  }
}

// 外部应用跳转的方法
void navigatePage(
  BuildContext context,
  Uri uri,
) {
  Timer.run(() {
    switch (uri.path) {
      case '/person':
      case '/contracts':
      case '/home':
        switchTab(context, uri.path);
        break;
      default:
        Navigator.pushNamed(context, uri.path);
    }
  });
}
