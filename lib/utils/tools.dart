import 'package:flutter/material.dart';
import '../constants/common.dart';

// 切换tabBar的方法
void switchTab(context, path, {arguments}) {
  if (path == null) return;
  if (arguments != null) {
    Navigator.of(context).pushReplacementNamed('/',
        arguments: {"index": tabBarMap[path], ...arguments});
  } else {
    Navigator.of(context)
        .pushReplacementNamed('/', arguments: {"index": tabBarMap[path]});
  }
}
