enum AlertAction {
  cancel,
  discard,
  disagree,
  agree,
}

const String apiURL = "http://channel-demo.test.ess.tencent.cn/api/";
const bool devMode = false;
const double textScaleFactor = 1.0;

// 首页tabBar控制页面的map
Map tabBarMap = {'/home': 0, '/contracts': 1, '/person': 2};
