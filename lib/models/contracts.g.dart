// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contracts.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

// const obj = {
//   "CreateOn": 1672912947,
//   "CustomData": "",
//   "DeadLine": 1704470400,
//   "FlowId": "yDwcEUUgyg331zy8UyGyVsBStKXQ1816",
//   "FlowMessage": "",
//   "FlowName": "梁盼的劳动合同_2023-01-05",
//   "FlowStatus": "ALL",
//   "FlowType": "",
//   "SignBeanTag": 0,
//   "UserId": "be33d3d4-932b-473a-b748-e3490148b4a0",
//   "id": "yDwcEUUgyg331zy8UyGyVsBStKXQ1816",
//   "ApplicationId": "yDRsbUUgyg1uget5Uy8lsS7ZfBvipOMJ",
//   "ProxyOrganizationOpenId": "f289e2c9-9d10-4ddd-85c3-095237de6c97",
//   "CustomerData": "",
//   "Deadline": 1704470400,
//   "OccurTime": 1672913123,
//   "FlowApproverInfos": [
//     {
//       "ApproveMessage": "",
//       "ApproveName": "杨秋硕",
//       "ApproveStatus": "ACCEPT",
//       "ApproveTime": 0,
//       "ApproveType": "ORGANIZATION",
//       "Mobile": "18601795619",
//       "ProxyOperatorOpenId": "be33d3d4-932b-473a-b748-e3490148b4a0",
//       "ProxyOrganizationName": "杨秋硕的测试企业8",
//       "ProxyOrganizationOpenId": "f289e2c9-9d10-4ddd-85c3-095237de6c97",
//       "ReceiptId": "yDwcuUUgyg31xgehUEFzhCo82sgBONLZ",
//       "SignOrder": 0
//     },
//     {
//       "ApproveMessage": "",
//       "ApproveName": "梁盼",
//       "ApproveStatus": "ACCEPT",
//       "ApproveTime": 0,
//       "ApproveType": "PERSON",
//       "Mobile": "13307143210",
//       "ProxyOperatorOpenId": "oHeIG5or_23VV59yUm7DHQSEM4lE",
//       "ProxyOrganizationName": "",
//       "ProxyOrganizationOpenId": "",
//       "ReceiptId": "yDwcuUUgyg31xgerUEFzhCoyR7fHMWBT",
//       "SignOrder": 0
//     }
//   ],
//   "workerId": "6a582ca2-b2d7-4921-bb50-1624ecab8073",
//   "workerName": "梁盼",
//   "workerMobile": "13307143210",
//   "workerGender": "FEMALE",
//   "workStatus": ""
// };

Contracts _$ContractsFromJson(Map<String, dynamic> json) {
  return Contracts()
    ..id = json['id'] as String
    ..flowId = json['FlowId'] as String
    ..flowName = json['FlowName'] as String
    ..flowStatus = json['FlowStatus'] as String
    ..flowType = json['FlowType'] as String
    ..flowMessage = json['FlowMessage'] as String
    ..flowApproverInfos = json['FlowApproverInfos'] as Object
    ..deadline = json['Deadline'] as String
    ..createdOn = json['CreatedOn'] as String
    ..userId = json['UserId'] as String
    ..applicationId = json['ApplicationId'] as String
    ..proxyOrganizationOpenId = json['ProxyOrganizationOpenId'] as String
    ..workerId = json['workerId'] as String
    ..workerName = json['workerName'] as String
    ..workerMobile = json['workerMobile'] as String
    ..workerGender = json['workerGender'] as String
    ..workStatus = json['workStatus'] as String;
}

Map<String, dynamic> _$ContractsToJson(Contracts instance) => <String, dynamic>{
      'id': instance.id,
      'flowId': instance.flowId,
      'flowName': instance.flowName,
      'flowStatus': instance.flowStatus,
      'flowType': instance.flowType,
      'flowMessage': instance.flowMessage,
      'flowApproverInfos': instance.flowApproverInfos,
      'deadline': instance.deadline,
      'createdOn': instance.createdOn,
      'userId': instance.userId,
      'applicationId': instance.applicationId,
      'proxyOrganizationOpenId': instance.proxyOrganizationOpenId,
      'workerId': instance.workerId,
      'workerName': instance.workerName,
      'workerMobile': instance.workerMobile,
      'workerGender': instance.workerGender,
      'workStatus': instance.workStatus,
    };
