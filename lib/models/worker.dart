import 'package:json_annotation/json_annotation.dart';

part 'worker.g.dart';

@JsonSerializable()
class Worker {
  Worker();
  // {
  //     required this.id,
  //     required this.name,
  //     required this.mobile,
  //     required this.gender,
  //     required this.creatorOpenId,
  //     required this.organizationId,
  //     required this.status,
  //     required this.createdOn,
  //   }

  @JsonKey(name: "id")
  String id = '';

  @JsonKey(name: "name")
  String name = '';

  @JsonKey(name: "mobile")
  String mobile = '';

  @JsonKey(name: "gender")
  String gender = '';

  @JsonKey(name: "age")
  int age = 0;

  @JsonKey(name: "creatorOpenId")
  String creatorOpenId = '';

  @JsonKey(name: "organizationId")
  String organizationId = '';

  @JsonKey(name: "status")
  String status = '';

  @JsonKey(name: "createdOn")
  String createdOn = '';

  // 默认头像
  final String avatar = 'r';

  String token = '';

  factory Worker.fromJson(Map<String, dynamic> json) => _$WorkerFromJson(json);

  Map<String, dynamic> toJson() => _$WorkerToJson(this);
}
