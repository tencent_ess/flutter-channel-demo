// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'organization.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Organization _$OrganizationFromJson(Map<String, dynamic> json) => Organization(
      json['id'] as String,
      json['organizationName'] as String,
      json['organizationOpenId'] as String,
      json['uniformSocialCreditCode'] as String,
      (json['proxyAppId'] ?? '') as String,
      json['workerId'] as String,
    );

Map<String, dynamic> _$OrganizationToJson(Organization instance) =>
    <String, dynamic>{
      'id': instance.id,
      'organizationName': instance.organizationName,
      'organizationOpenId': instance.organizationOpenId,
      'uniformSocialCreditCode': instance.uniformSocialCreditCode,
      'proxyAppId': instance.proxyAppId,
      'workerId': instance.workerId,
    };
