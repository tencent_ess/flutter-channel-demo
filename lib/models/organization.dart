import 'package:json_annotation/json_annotation.dart';

part 'organization.g.dart';

@JsonSerializable()
class Organization {
  final String id;
  final String organizationName;
  final String organizationOpenId;
  final String uniformSocialCreditCode;
  final String proxyAppId;
  final String workerId;
  Organization(this.id, this.organizationName, this.organizationOpenId,
      this.uniformSocialCreditCode, this.proxyAppId, this.workerId);

  factory Organization.fromJson(Map<String, dynamic> json) =>
      _$OrganizationFromJson(json);
  Map<String, dynamic> toJson() => _$OrganizationToJson(this);
}
