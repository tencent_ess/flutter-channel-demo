// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'send.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Send _$SendFromJson(Map<String, dynamic> json) {
  return Send()
    ..id = json['id'] as String
    ..startTime = json['startTime'] as String
    ..endTime = json['endTime'] as String
    ..tenantName = json['tenantName'] as String
    ..workerId = json['workerId'] as String
    ..tenantId = json['tenantId'] as String
    ..status = json['status'] as String
    ..createdOn = json['createdOn'] as String;
}

Map<String, dynamic> _$SendToJson(Send instance) => <String, dynamic>{
      'id': instance.id,
      'tenantName': instance.tenantName,
      'startTime': instance.startTime,
      'endTime': instance.endTime,
      'workerId': instance.workerId,
      'tenantId': instance.tenantId,
      'status': instance.status,
      'createdOn': instance.createdOn,
    };
