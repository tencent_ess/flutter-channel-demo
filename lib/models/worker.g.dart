// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'worker.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

// const a = {
//   "name": "迷路了娜娜",
//   "mobile": "17878786767",
//   "gender": "FEMALE",
//   "creatorOpenId": "5d18c1c4-9f42-4b3e-a990-3a0c8926cd11",
//   "organizationId": "572c69b2-33ec-44cf-aa0c-14e06d1245e4",
//   "id": "be763df9-3685-40c0-8dcc-72882dde59f8",
//   "createdOn": "2023-01-07T05:51:19.454Z",
//   "status": "INIT"
// };

Worker _$WorkerFromJson(Map<String, dynamic> json) {
  return Worker()
    ..id = json['id'] as String
    ..name = json['name'] as String
    ..mobile = json['mobile'] as String
    ..gender = json['gender'] as String
    ..age = (json['age'] ?? 0) as int
    ..creatorOpenId = json['creatorOpenId'] as String
    ..organizationId = json['organizationId'] as String
    ..status = json['status'] as String
    ..createdOn = json['createdOn'] as String;
}

Map<String, dynamic> _$WorkerToJson(Worker instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'mobile': instance.mobile,
      'gender': instance.gender,
      'age': instance.age,
      'creatorOpenId': instance.creatorOpenId,
      'organizationId': instance.organizationId,
      'status': instance.status,
      'createdOn': instance.createdOn,
    };
