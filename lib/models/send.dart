import 'package:json_annotation/json_annotation.dart';

part 'send.g.dart';

@JsonSerializable()
class Send {
  Send();

  @JsonKey(name: "id")
  String id = '';
  @JsonKey(name: "tenantName")
  String tenantName = '';

  @JsonKey(name: "startTime")
  String startTime = '';

  @JsonKey(name: "endTime")
  String endTime = '';

  @JsonKey(name: "tenantId")
  String tenantId = '';

  @JsonKey(name: "workerId")
  String workerId = '';

  @JsonKey(name: "createdOn")
  String createdOn = '';

  @JsonKey(name: "status")
  String status = '';

  String token = '';

  factory Send.fromJson(Map<String, dynamic> json) => _$SendFromJson(json);

  Map<String, dynamic> toJson() => _$SendToJson(this);
}
