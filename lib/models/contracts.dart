import 'package:json_annotation/json_annotation.dart';

part 'contracts.g.dart';

// const obj = {
//   "CreateOn": 1672912947,
//   "CustomData": "",
//   "DeadLine": 1704470400,
//   "FlowId": "yDwcEUUgyg331zy8UyGyVsBStKXQ1816",
//   "FlowMessage": "",
//   "FlowName": "梁盼的劳动合同_2023-01-05",
//   "FlowStatus": "ALL",
//   "FlowType": "",
//   "SignBeanTag": 0,
//   "UserId": "be33d3d4-932b-473a-b748-e3490148b4a0",
//   "id": "yDwcEUUgyg331zy8UyGyVsBStKXQ1816",
//   "ApplicationId": "yDRsbUUgyg1uget5Uy8lsS7ZfBvipOMJ",
//   "ProxyOrganizationOpenId": "f289e2c9-9d10-4ddd-85c3-095237de6c97",
//   "CustomerData": "",
//   "Deadline": 1704470400,
//   "OccurTime": 1672913123,
//   "FlowApproverInfos": [
//     {
//       "ApproveMessage": "",
//       "ApproveName": "杨秋硕",
//       "ApproveStatus": "ACCEPT",
//       "ApproveTime": 0,
//       "ApproveType": "ORGANIZATION",
//       "Mobile": "18601795619",
//       "ProxyOperatorOpenId": "be33d3d4-932b-473a-b748-e3490148b4a0",
//       "ProxyOrganizationName": "杨秋硕的测试企业8",
//       "ProxyOrganizationOpenId": "f289e2c9-9d10-4ddd-85c3-095237de6c97",
//       "ReceiptId": "yDwcuUUgyg31xgehUEFzhCo82sgBONLZ",
//       "SignOrder": 0
//     },
//     {
//       "ApproveMessage": "",
//       "ApproveName": "梁盼",
//       "ApproveStatus": "ACCEPT",
//       "ApproveTime": 0,
//       "ApproveType": "PERSON",
//       "Mobile": "13307143210",
//       "ProxyOperatorOpenId": "oHeIG5or_23VV59yUm7DHQSEM4lE",
//       "ProxyOrganizationName": "",
//       "ProxyOrganizationOpenId": "",
//       "ReceiptId": "yDwcuUUgyg31xgerUEFzhCoyR7fHMWBT",
//       "SignOrder": 0
//     }
//   ],
//   "workerId": "6a582ca2-b2d7-4921-bb50-1624ecab8073",
//   "workerName": "梁盼",
//   "workerMobile": "13307143210",
//   "workerGender": "FEMALE",
//   "workStatus": ""
// };

@JsonSerializable()
class Contracts {
  Contracts();

  @JsonKey(name: "id")
  String id = '';

  @JsonKey(name: "FlowId")
  String flowId = '';

  @JsonKey(name: "FlowName")
  String flowName = '';

  @JsonKey(name: "FlowStatus")
  String flowStatus = '';

  @JsonKey(name: "FlowType")
  String flowType = '';

  @JsonKey(name: "FlowMessage")
  String flowMessage = '';

  @JsonKey(name: "FlowApproverInfos")
  Object flowApproverInfos = [];

  @JsonKey(name: "Deadline")
  String deadline = '';

  @JsonKey(name: "CreateOn")
  String createdOn = '';

  @JsonKey(name: "UserId")
  String userId = '';

  @JsonKey(name: "ApplicationId")
  String applicationId = '';

  @JsonKey(name: "ProxyOrganizationOpenId")
  String proxyOrganizationOpenId = '';

  @JsonKey(name: "workerId")
  String workerId = '';

  @JsonKey(name: "workerName")
  String workerName = '';

  @JsonKey(name: "workerMobile")
  String workerMobile = '';

  @JsonKey(name: "workerGender")
  String workerGender = '';

  @JsonKey(name: "workStatus")
  String workStatus = '';

  String token = '';

  factory Contracts.fromJson(Map<String, dynamic> json) =>
      _$ContractsFromJson(json);

  Map<String, dynamic> toJson() => _$ContractsToJson(this);
}
