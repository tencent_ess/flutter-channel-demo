import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import '../common/HttpRequest.dart';

var dio = HttpRequest.dio;

// 获取合同列表
fetchTemplateList(contractType, limit, offset, BuildContext context) async {
  var res = await dio.post('/template/describeTemplates',
      data: {
        "limit": limit,
        "offset": offset,
        "templateName": "",
        "tenantTemplateType": 0,
        "withPreviewUrl": true
      },
      options: Options(extra: {"noCache": false, "context": context}));
  return res.data;
}
