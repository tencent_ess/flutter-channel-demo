import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import '../common/HttpRequest.dart';
import 'package:flutter_channel_demo/common/Global.dart';

var dio = HttpRequest.dio;

fetchList(offset, tenantId, BuildContext context) async {
  var res = await dio.post('/send/list',
      data: {
        "workerId": Global.worker.token,
        "needTenantInfo": true,
        "limit": 10,
        "offset": offset,
        "tenantId": tenantId,
      },
      options: Options(extra: {"noCache": true, "context": context}));
  return res.data;
}

fetchHomeSend() async {
  var res = await dio.post('/send/listForHome',
      data: {
        "workerId": Global.worker.token,
      },
      options: Options(extra: {"noCache": true}));
  return res.data;
}

fetchWorkerTenants(BuildContext context) async {
  var res = await dio.post('/worker/getTenants',
      data: {
        "workerId": Global.worker.token,
      },
      options: Options(extra: {"noCache": false, "context": context}));
  return res.data;
}
