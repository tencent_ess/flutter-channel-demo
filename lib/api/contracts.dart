import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import '../common/HttpRequest.dart';

var dio = HttpRequest.dio;

// 获取合同列表
fetchContractList(
    contractType, limit, offset, status, BuildContext context) async {
  var res = await dio.post('/contracts/list',
      data: {
        "contractType": contractType,
        "limit": limit,
        "offset": offset,
        "status": status,
      },
      options: Options(extra: {"noCache": false, "context": context}));
  return res.data;
}

// 生成h5链接
createH5SignUrl(params, BuildContext context) async {
  var res = await dio.post('/contracts/createSignUrl',
      data: params,
      options: Options(extra: {"noCache": false, "context": context}));
  return res.data;
}
