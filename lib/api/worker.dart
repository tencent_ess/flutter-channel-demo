import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import '../common/HttpRequest.dart';

var dio = HttpRequest.dio;

login(mobile, pwd, BuildContext context) async {
  var res = await dio.post('/user/mLogin',
      data: {
        "mobile": mobile,
        "password": pwd,
      },
      options: Options(extra: {"noCache": true, "context": context}));
  return res.data;
}

getInfo() async {
  var res = await dio.get('/user/info');
  return res.data;
}

fetchList() async {
  var res = await dio.get('/worker/list');
  return res.data;
}

logout() async {
  var res = await dio.post('/logout');
  return res.data;
}

getWorkerOrganizations() async {
  var res = await dio.post('/user/getWorkerOrganizations');
  return res.data;
}
