import 'dart:convert';

import 'package:flutter_channel_demo/models/worker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import './NetCache.dart';
import './HttpRequest.dart';

class Global {
  static late final SharedPreferences _prefs;

  static Worker worker = Worker();

  // 扩展缓存网络对象
  static NetCache netCache = NetCache();

  // 是否为release版
  static bool get isRelease => const bool.fromEnvironment("dart.vm.product");

  //初始化全局信息，会在APP启动时执行，从缓存读取工人信息
  static Future init() async {
    _prefs = await SharedPreferences.getInstance();
    var _worker = _prefs.getString("worker");
    print(_worker);
    if (_worker != null) {
      try {
        worker = Worker.fromJson(jsonDecode(_worker));
      } catch (e) {
        print(e);
      }
    }

    HttpRequest.init();
  }

  // 持久化Worker信息
  static saveProfile() =>
      _prefs.setString("worker", jsonEncode(worker.toJson()));
}
