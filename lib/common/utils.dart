import 'package:flutter/widgets.dart';

class CustomNavigatorObserver extends NavigatorObserver {
  static CustomNavigatorObserver? _instance;

  static CustomNavigatorObserver? getInstance() {
    // ignore: prefer_conditional_assignment
    if (_instance == null) {
      _instance = CustomNavigatorObserver();
    }
    return _instance;
  }
}
