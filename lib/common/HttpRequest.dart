import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import './Global.dart';

class HttpRequest {
  BuildContext? context;
  Options? _options;

  // 在网络请求过程中可能会需要使用当前的context信息，比如在请求失败时
  // 打开一个新路由，而打开新路由需要context信息。
  HttpRequest([this.context]) {
    _options = Options(extra: {"context": context});
  }

  // 初始化请求，配置默认headers
  static Dio dio = Dio(BaseOptions(
      // baseUrl: 'http://localhost:9527/h5-api/',
      baseUrl: 'https://channel-demo.test.ess.tencent.cn/h5-api/',
      headers: {
        HttpHeaders.acceptHeader: "application/json; charset=utf-8",
      }));

  static void init() {
    // 添加缓存插件
    dio.interceptors.add(Global.netCache);
    dio.options.headers['x-token'] = Global.worker.token;
  }
}
