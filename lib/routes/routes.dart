import 'package:flutter/material.dart';
import 'package:flutter_channel_demo/pages/tabs/tabs.dart';
import 'package:flutter_channel_demo/pages/login/login.dart';
import 'package:flutter_channel_demo/pages/webview/webview.dart';
import 'package:flutter_channel_demo/pages/sendlist/sendlist.dart';
import 'package:flutter_channel_demo/common/Global.dart';

Map routes = {
  '/': (context, {arguments}) => Tabs(arguments: arguments),
  "/login": (BuildContext context) => const Login(),
  '/webview': (context, {arguments}) => WebviewPage(arguments: arguments),
  '/sendlist': (context) => const SendListPage(),
};

String? routeBeforeHook(RouteSettings settings) {
  final token = Global.worker.token;
  if (token != '') {
    if (settings.name == 'login') {
      return 'index';
    }
    return settings.name;
  }
  return 'login';
}

var onGenerateRoute = (RouteSettings settings) {
  final String? name = settings.name;
  final Function? pageContentBuilder = routes[name];

  String? routeName = routeBeforeHook(settings);

  if (routeName == 'login') {
    final Route route = MaterialPageRoute(builder: (context) => const Login());
    return route;
  }

  if (pageContentBuilder != null) {
    if (settings.arguments != null) {
      final Route route = MaterialPageRoute(
          builder: (context) =>
              pageContentBuilder(context, arguments: settings.arguments));
      return route;
    } else {
      final Route route =
          MaterialPageRoute(builder: (context) => pageContentBuilder(context));

      return route;
    }
  }
  return null;
};
