import 'package:flutter/material.dart';
import 'package:flutter_channel_demo/utils/tools.dart';

class MyContracts extends StatelessWidget {
  final int count;

  const MyContracts({Key? key, required this.count}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.white,
      margin: const EdgeInsets.all(10),
      child: Container(
        padding: const EdgeInsets.all(16),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  '我的合同',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                GestureDetector(
                  onTap: () => switchTab(context, '/contracts'), // 跳转到合同页
                  child: const Icon(Icons.keyboard_arrow_right),
                ),
              ],
            ),
            const SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: GestureDetector(
                    child: Column(
                      children: [
                        Stack(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Icon(
                                Icons.document_scanner,
                                size: 50,
                              ),
                            ),
                            if (count > 0)
                              Positioned(
                                top: 0,
                                right: 5,
                                child: ClipOval(
                                  child: Container(
                                    decoration: const BoxDecoration(
                                      color: Colors.red,
                                    ),
                                    padding: const EdgeInsets.all(4),
                                    child: Text(
                                      count.toString(),
                                      style: const TextStyle(
                                        fontSize: 12,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                          ],
                        ),
                        const SizedBox(height: 10),
                        const Text('待签署'),
                      ],
                    ),
                    onTap: () => switchTab(context, '/contracts',
                        arguments: {'status': 'WAITSIGN'}),
                  ),
                ),
                const SizedBox(width: 20),
                Expanded(
                  child: GestureDetector(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Icon(
                            Icons.file_download_done_rounded,
                            size: 50,
                          ),
                        ),
                        const SizedBox(height: 10),
                        const Text('已签署'),
                      ],
                    ),
                    onTap: () => switchTab(context, '/contracts',
                        arguments: {'status': 'SIGNED'}),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
