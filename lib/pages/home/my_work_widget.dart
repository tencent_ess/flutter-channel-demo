import 'package:flutter/material.dart';
import 'package:flutter_channel_demo/widgets/sendItem.dart';

class MyWork extends StatelessWidget {
  final List workList;
  final bool loading;

  const MyWork({super.key, required this.workList, required this.loading});

  // 列表内容区
  Widget contentBuilder(context) {
    if (workList.isNotEmpty) {
      return Column(
        children: workList
            .map((item) => sendItem(context: context, item: item))
            .toList(),
      );
    }
    return Column(
      children: [
        Image.network(
          'https://img01.yzcdn.cn/vant/empty-image-default.png',
          width: 150,
          height: 150,
        ),
        const Text(
          "暂无工作记录",
          style: TextStyle(color: Color(0xff969799), fontSize: 14),
        ),
      ],
    );
  }

  Widget build(BuildContext context) {
    return Card(
        color: Colors.white,
        margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
        child: Container(
            padding: const EdgeInsets.all(16),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text('我的工作',
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold)),
                    GestureDetector(
                        onTap: () =>
                            Navigator.pushNamed(context, '/test'), // 跳转到派遣记录
                        child: const Icon(Icons.keyboard_arrow_right)),
                  ],
                ),
                Container(
                    padding: const EdgeInsets.only(top: 10, bottom: 10),
                    constraints: const BoxConstraints(minHeight: 170),
                    child: loading ? loadingView() : contentBuilder(context))
              ],
            )));
  }
}

/// 加载中 View
Widget loadingView() {
  return const Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        CircularProgressIndicator(
          strokeWidth: 2,
          valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
        )
      ]);
}
