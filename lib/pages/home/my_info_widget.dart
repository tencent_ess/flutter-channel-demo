import 'package:flutter/material.dart';
import 'package:flutter_channel_demo/utils/tools.dart';

class MyInfo extends StatelessWidget {
  final String selectedOrg;
  final String gender;
  final String username;
  final Function showModalPopup;

  const MyInfo(
      {super.key,
      required this.selectedOrg,
      required this.username,
      required this.gender,
      required this.showModalPopup});

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 200,
        decoration: const BoxDecoration(color: Color(0xff334154)),
        child: GestureDetector(
            onTap: () => switchTab(context, '/person'), // 替换路由跳转到我的页面
            child: Column(
              children: [
                Container(
                    alignment: Alignment.center,
                    child: GestureDetector(
                      onTap: () => showModalPopup(context),
                      child: Container(
                          padding: const EdgeInsets.only(top: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                selectedOrg,
                                style: const TextStyle(color: Colors.white),
                              ),
                              const Icon(
                                Icons.arrow_drop_down,
                                size: 20,
                                color: Colors.white,
                              )
                            ],
                          )),
                    )),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(20),
                      child: CircleAvatar(
                          radius: 44,
                          backgroundImage: gender == 'MALE'
                              ? const NetworkImage(
                                  'https://channel-demo.test.ess.tencent.cn/static/img/male.6970a838.jpg')
                              : const NetworkImage(
                                  'https://channel-demo.test.ess.tencent.cn/static/img/female.6d657899.jpg')),
                    ),
                    Expanded(
                        flex: 1,
                        child: Text(username,
                            style: const TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.bold))),
                    const Icon(
                      Icons.keyboard_arrow_right,
                      size: 30,
                      color: Colors.white,
                    )
                  ],
                ),
              ],
            )));
  }
}
