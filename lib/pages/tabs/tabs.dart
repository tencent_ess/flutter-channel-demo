import 'package:flutter/material.dart';
import 'package:flutter_channel_demo/pages/home/home.dart';
import 'package:flutter_channel_demo/pages/person/person.dart';
import 'package:flutter_channel_demo/pages/contracts/contracts.dart';

class Tabs extends StatefulWidget {
  final Map? arguments;
  const Tabs({super.key, this.arguments});

  @override
  State<Tabs> createState() => _TabsState();
}

class _TabsState extends State<Tabs> {
  int _selectedIndex = 0;
  late List<Widget> _widgetOptions;
  //定义页面控制器，可以左右滑动切换页面
  PageController pageController = PageController(initialPage: 0);

  @override
  void initState() {
    // 通过路由参数改变tab的选中，从而改变页面
    if (widget.arguments != null) {
      setState(() {
        _selectedIndex = widget.arguments?['index'];
        _widgetOptions = [
          const Home(),
          Contracts(
            arguments: widget.arguments,
          ),
          const Person(),
        ];
      });
    }
    super.initState();
  }

  void _onItemTaped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: IndexedStack(
          index: _selectedIndex,
          children: _widgetOptions,
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: const [
            BottomNavigationBarItem(icon: Icon(Icons.home), label: '首页'),
            BottomNavigationBarItem(icon: Icon(Icons.copy), label: '合同'),
            BottomNavigationBarItem(icon: Icon(Icons.person), label: '我的'),
          ],
          currentIndex: _selectedIndex,
          onTap: _onItemTaped,
        ));
  }
}
