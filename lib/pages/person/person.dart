import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_channel_demo/models/worker.dart';
import 'package:flutter_channel_demo/common/Global.dart';

class Person extends StatefulWidget {
  const Person({super.key});

  @override
  State<Person> createState() => _PersonState();
}

class _PersonState extends State<Person> with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true; // 是否开启缓存
  late String _username = '';
  late String _phone = '';
  late String _sex = '';
  late int _age = 0;

  @override
  void initState() {
    super.initState();
    _getWorker();
  }

  _getWorker() async {
    var preferences = await SharedPreferences.getInstance();
    var worker = preferences.getString("worker");
    if (worker != null) {
      try {
        var res = Worker.fromJson(json.decode(worker));
        setState(() {
          _username = res.name;
          _phone = res.mobile;
          _sex = res.gender != "MALE" ? '女' : '男';
          _age = res.age;
        });
      } catch (e) {
        debugPrint(e as String);
        return null;
      }
    }
    return null;
  }

  void _logout() {
    Global.worker.token = '';
    SharedPreferences.getInstance().then((pref) {
      pref.setString("token", '');
      pref.setString("worker", '');
    });
    Navigator.pushNamed(context, '/login');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('用户中心页')),
        body: Padding(
          padding: const EdgeInsets.all(12),
          child: ListView(
            children: [
              ListTile(
                title: Text(
                  '姓名',
                  style: Theme.of(context).textTheme.titleMedium,
                ),
                trailing: Text(
                  _username,
                  style: Theme.of(context).textTheme.labelMedium,
                ),
              ),
              const Divider(),
              ListTile(
                title: Text(
                  '手机号',
                  style: Theme.of(context).textTheme.titleMedium,
                ),
                trailing: Text(
                  _phone,
                  style: Theme.of(context).textTheme.labelMedium,
                ),
              ),
              const Divider(),
              ListTile(
                title: Text(
                  '年龄',
                  style: Theme.of(context).textTheme.titleMedium,
                ),
                trailing: Text(
                  _age.toString(),
                  style: Theme.of(context).textTheme.labelMedium,
                ),
              ),
              if (_age > 0) const Divider(),
              Container(
                margin: const EdgeInsets.only(top: 44),
                height: 44,
                width: double.infinity,
                child: ElevatedButton(
                  onPressed: _logout,
                  child: const Text('退出登录'),
                ),
              )
            ],
          ),
        ));
  }
}
