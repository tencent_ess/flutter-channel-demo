package con.ess.flutter.channel;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebSettings;
import android.webkit.WebViewClient;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import io.flutter.plugin.platform.PlatformView;
import java.util.Map;

import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.PermissionRequest;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.app.AlertDialog;
import android.util.Log;
import android.net.http.SslError;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceRequest;

class NativeView implements PlatformView {
    private static final String TAG = "NativeView";
   @NonNull private final WebView mWebView;
   private H5FaceWebChromeClient webViewClient;

   NativeView(@NonNull Context context, MainActivity activity, int id, @Nullable Map<String, Object> creationParams) {
    mWebView = new WebView(context);
    String url = (String) creationParams.get("url");

    mWebView.setWebViewClient(new WebViewClient() {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
               Log.d(TAG, "shouldOverrideUrlLoading():" + view.getUrl());
            String url = view.getUrl();
            return super.shouldOverrideUrlLoading(view, request);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
               Log.d(TAG, "onPageFinished():" + url);
        }

        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
            Log.e(TAG, "webview访问网址ssl证书无效！询问客户");
            handler.proceed();
        }
    });
    // webViewClient = new H5FaceWebChromeClient(activity);
    mWebView.setWebChromeClient(activity.webViewClient);

   

    WebSettings webSettings = mWebView.getSettings();
 

    webSettings.setDomStorageEnabled(true);
    webSettings.setJavaScriptEnabled(true);

    WBH5FaceVerifySDK.getInstance().setWebViewSettings(mWebView, context);
    Log.d(TAG, "url ==="+ url);
    mWebView.loadUrl(url);
}

    @NonNull
    @Override
    public View getView() {
        return mWebView;
    }

    @Override
    public void dispose() {}

    
}