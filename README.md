# flutter_channel_demo

A new Flutter project.

## Getting Started

先打开模拟器

```  open -a Simulator ```

再启动命令

``` flutter run --no-sound-null-safety ```

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.


## 跳转TabBar页面的方法
1. 官方方法：Navigator.of(context).pushReplacementNamed('/',
        arguments: {"index": 0});
2. 引入utils/tools.dart， 然后调用switchTab(context, '/home', arguments: arguments); 其中‘/home’指向首页，'/contracts'指向合同页，'/person'指向我的信息页